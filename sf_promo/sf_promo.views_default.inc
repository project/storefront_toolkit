<?php
/**
 * @file
 * sf_promo.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sf_promo_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'promos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Promos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Promo Image */
  $handler->display->display_options['fields']['field_promo_image']['id'] = 'field_promo_image';
  $handler->display->display_options['fields']['field_promo_image']['table'] = 'field_data_field_promo_image';
  $handler->display->display_options['fields']['field_promo_image']['field'] = 'field_promo_image';
  $handler->display->display_options['fields']['field_promo_image']['label'] = '';
  $handler->display->display_options['fields']['field_promo_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_promo_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_promo_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promo_image']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_promo_image']['element_wrapper_class'] = 'right';
  $handler->display->display_options['fields']['field_promo_image']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_promo_image']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_promo_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_promo_image']['settings'] = array(
    'image_style' => 'promo',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_promo_image']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_wrapper_class'] = 'headline';
  $handler->display->display_options['fields']['title']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 0;
  /* Field: Content: Subtitle */
  $handler->display->display_options['fields']['field_subtitle']['id'] = 'field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['table'] = 'field_data_field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['field'] = 'field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['label'] = '';
  $handler->display->display_options['fields']['field_subtitle']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_subtitle']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_subtitle']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_subtitle']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_subtitle']['element_wrapper_class'] = 'subtitle';
  $handler->display->display_options['fields']['field_subtitle']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_subtitle']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_subtitle']['field_api_classes'] = 0;
  /* Field: Content: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['label'] = '';
  $handler->display->display_options['fields']['field_message']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_message']['element_wrapper_class'] = 'message';
  $handler->display->display_options['fields']['field_message']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_message']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message']['field_api_classes'] = 0;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_link']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_link']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_link']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_link']['element_wrapper_class'] = 'promo-link';
  $handler->display->display_options['fields']['field_link']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_link']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_link']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_link']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link']['field_api_classes'] = 0;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['external'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['html'] = 0;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['counter']['element_wrapper_class'] = 'pager-item';
  $handler->display->display_options['fields']['counter']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['counter']['hide_empty'] = 0;
  $handler->display->display_options['fields']['counter']['empty_zero'] = 0;
  $handler->display->display_options['fields']['counter']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Content: Position */
  $handler->display->display_options['fields']['field_position']['id'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['table'] = 'field_data_field_position';
  $handler->display->display_options['fields']['field_position']['field'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['label'] = '';
  $handler->display->display_options['fields']['field_position']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_position']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_position']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_position']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_position']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_position']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_position']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_position']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_position']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_position']['field_api_classes'] = 0;
  /* Sort criterion: Content: Position (field_position) */
  $handler->display->display_options['sorts']['field_position_value']['id'] = 'field_position_value';
  $handler->display->display_options['sorts']['field_position_value']['table'] = 'field_data_field_position';
  $handler->display->display_options['sorts']['field_position_value']['field'] = 'field_position_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'promo' => 'promo',
  );

  /* Display: Promo Block */
  $handler = $view->new_display('block', 'Promo Block', 'block');
  $export['promos'] = $view;

  return $export;
}
