<?php
/**
 * @file
 * sf_product_detail.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function sf_product_detail_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'product_details';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Product Details';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'product_details',
      'display' => 'block',
      'args' => '',
      'title' => 'Description',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'product_detail_reviews',
      'display' => 'block',
      'args' => '',
      'title' => 'Reviews',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Description');
  t('Product Details');
  t('Reviews');

  $export['product_details'] = $quicktabs;

  return $export;
}
