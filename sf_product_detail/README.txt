*******************************************************************************
                   DRUPAL MODULE
*******************************************************************************
Name: Storefront Toolkit: Product Detail
Maintainers: Aaron Dudenhofer (dudenhofer), Dave Pullen (AngryWookie)
Version: 7.x-1.x

*******************************************************************************
OVERVIEW:
Builds a new product display packed with features like social links, rating, and
image gallery. Note that the current stable version of Colorbox (7.x-1.3) has
undergone significant code changes when compared with the dev version. We
recommend using the latest dev version of Colorbox for now.

*******************************************************************************
HOW IT WORKS:

Creates a 'Product Detail' content type. This is the product's display node and
it's important to note that this is a separate display from the product
displays provided by Commerce or displays you might have created previously. This
means that you cannot reference images from current products, but must upload new
(or the same) images when creating content of type 'Product Detail'. The plus side
is that now you can have multiple displays for the same product type
e.g. a 'General Product Display' and 'Product Detail Display'.

Creates a 'Product Detail Reviews' block View. This block is displayed only when
a node of type 'Product Detail' is being viewed (using the 'Context' module),
and allows users to write product reviews and rate the product.

*******************************************************************************
INSTALLATION:

Create two new folders in /sites/all/libraries called:
  i)   jquery.cycle
  ii)  jquery.jcarousel

Add a sub directory 'lib' in jquery.jcarousel, (i.e. jquery.jcarousel/lib)

Download the JQuery Cycle plugin (it's a single file: jquery.cycle.all.js) here:
Link:  http://jquery.malsup.com/cycle/download.html
(don't choose the Lite version), and move the file into /sites/all/libraries/jquery.cycle/

If you're planning to use a carousel for pager, download the JCarousel plugin
here : http://sorgalla.com/jcarousel
and move lib/jquery.jcarousel.js (or lib/jquery.jcarousel.min.js) file into
/sites/all/libraries/jquery.jcarousel/lib

Note: One of Product Details dependencies (Colorbox module) requires the colorbox
plugin. Download and extract the entire contents of the archive into
/sites/all/libraries/colorbox e.g. colorbox/colorbox, colorbox/content etc.
Link:  http://colorpowered.com/colorbox/colorbox.zip

*******************************************************************************
