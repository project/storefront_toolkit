<?php
/**
 * @file
 * sf_product_detail.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sf_product_detail_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sf_product_detail';
  $context->description = '';
  $context->tag = 'sf';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product_detail' => 'product_detail',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-product_details' => array(
          'module' => 'quicktabs',
          'delta' => 'product_details',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sf');
  $export['sf_product_detail'] = $context;

  return $export;
}
