<?php
/**
 * @file
 * sf_product_detail.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function sf_product_detail_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_detail|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_detail';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'write_review_link' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|product_detail|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function sf_product_detail_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'write_review_link';
  $ds_field->label = 'Write Review Link';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a class="add left" href="<?php print base_path(); ?>comment/reply/<?php print $entity->nid ?>#comment-form">Write a review</a>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['write_review_link'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function sf_product_detail_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_detail|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_detail';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => 0,
    'regions' => array(
      'left' => array(
        0 => 'field_images',
      ),
      'right' => array(
        0 => 'product:commerce_price',
        1 => 'field_rating',
        2 => 'field_product',
        3 => 'write_review_link',
      ),
    ),
    'fields' => array(
      'field_images' => 'left',
      'product:commerce_price' => 'right',
      'field_rating' => 'right',
      'field_product' => 'right',
      'write_review_link' => 'right',
    ),
    'classes' => array(),
  );
  $export['node|product_detail|full'] = $ds_layout;

  return $export;
}
