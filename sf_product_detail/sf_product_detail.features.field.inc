<?php
/**
 * @file
 * sf_product_detail.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function sf_product_detail_field_default_fields() {
  $fields = array();

  // Exported field: 'comment-comment_node_product_detail-field_rating'.
  $fields['comment-comment_node_product_detail-field_rating'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_rating',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'vote',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'comment_node_product_detail',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => TRUE,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => NULL,
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'comment',
      'field_name' => 'field_rating',
      'label' => 'Rating',
      'required' => 1,
      'settings' => array(
        'allow_clear' => 0,
        'stars' => '5',
        'target' => 'parent_node',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(
          'widget' => array(
            'fivestar_widget' => 'default',
          ),
        ),
        'type' => 'stars',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-product_detail-body'.
  $fields['node-product_detail-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'product_detail',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
        'full' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '15',
        ),
        'product_grid' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
        'product_list' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Body',
      'required' => FALSE,
      'settings' => array(
        'display_summary' => TRUE,
        'text_processing' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'rows' => 20,
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-product_detail-field_images'.
  $fields['node-product_detail-field_images'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_images',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'product_detail',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'field_slideshow',
          'settings' => array(
            'slideshow_caption' => 'title',
            'slideshow_caption_link' => '',
            'slideshow_carousel_circular' => 0,
            'slideshow_carousel_image_style' => '',
            'slideshow_carousel_scroll' => '1',
            'slideshow_carousel_speed' => '500',
            'slideshow_carousel_vertical' => 0,
            'slideshow_carousel_visible' => '3',
            'slideshow_colorbox_image_style' => '',
            'slideshow_colorbox_slideshow' => '',
            'slideshow_colorbox_slideshow_speed' => '4000',
            'slideshow_colorbox_speed' => '350',
            'slideshow_colorbox_transition' => 'elastic',
            'slideshow_controls' => 0,
            'slideshow_field_collection_image' => '',
            'slideshow_fx' => 'fade',
            'slideshow_image_style' => 'sf_main',
            'slideshow_link' => 'colorbox',
            'slideshow_order' => '',
            'slideshow_pager' => 'image',
            'slideshow_pager_image_style' => 'sf_thumb',
            'slideshow_pause' => 0,
            'slideshow_speed' => '1000',
            'slideshow_timeout' => '0',
          ),
          'type' => 'slideshow',
          'weight' => '0',
        ),
        'full' => array(
          'label' => 'hidden',
          'module' => 'field_slideshow',
          'settings' => array(
            'slideshow_caption' => '',
            'slideshow_caption_link' => '',
            'slideshow_carousel_circular' => 1,
            'slideshow_carousel_image_style' => 'product_detail_thumb',
            'slideshow_carousel_scroll' => '1',
            'slideshow_carousel_speed' => '500',
            'slideshow_carousel_vertical' => 0,
            'slideshow_carousel_visible' => '3',
            'slideshow_colorbox_image_style' => '',
            'slideshow_colorbox_slideshow' => 'manual',
            'slideshow_colorbox_slideshow_speed' => '4000',
            'slideshow_colorbox_speed' => '350',
            'slideshow_colorbox_transition' => 'elastic',
            'slideshow_controls' => 0,
            'slideshow_field_collection_image' => '',
            'slideshow_fx' => 'fade',
            'slideshow_image_style' => 'product_detail',
            'slideshow_link' => 'colorbox',
            'slideshow_order' => '',
            'slideshow_pager' => 'carousel',
            'slideshow_pager_image_style' => '',
            'slideshow_pause' => 0,
            'slideshow_speed' => '1000',
            'slideshow_timeout' => '4000',
          ),
          'type' => 'slideshow',
          'weight' => '0',
        ),
        'product_grid' => array(
          'label' => 'hidden',
          'module' => 'field_slideshow',
          'settings' => array(
            'slideshow_caption' => 'title',
            'slideshow_caption_link' => '',
            'slideshow_carousel_circular' => 0,
            'slideshow_carousel_image_style' => '',
            'slideshow_carousel_scroll' => '1',
            'slideshow_carousel_speed' => '500',
            'slideshow_carousel_vertical' => 0,
            'slideshow_carousel_visible' => '3',
            'slideshow_colorbox_image_style' => '',
            'slideshow_colorbox_slideshow' => '',
            'slideshow_colorbox_slideshow_speed' => '4000',
            'slideshow_colorbox_speed' => '350',
            'slideshow_colorbox_transition' => 'elastic',
            'slideshow_controls' => 0,
            'slideshow_field_collection_image' => '',
            'slideshow_fx' => 'fade',
            'slideshow_image_style' => 'sf_main',
            'slideshow_link' => 'colorbox',
            'slideshow_order' => '',
            'slideshow_pager' => 'image',
            'slideshow_pager_image_style' => 'sf_thumb',
            'slideshow_pause' => 0,
            'slideshow_speed' => '1000',
            'slideshow_timeout' => '0',
          ),
          'type' => 'slideshow',
          'weight' => '0',
        ),
        'product_list' => array(
          'label' => 'hidden',
          'module' => 'field_slideshow',
          'settings' => array(
            'slideshow_caption' => 'title',
            'slideshow_caption_link' => '',
            'slideshow_carousel_circular' => 0,
            'slideshow_carousel_image_style' => '',
            'slideshow_carousel_scroll' => '1',
            'slideshow_carousel_speed' => '500',
            'slideshow_carousel_vertical' => 0,
            'slideshow_carousel_visible' => '3',
            'slideshow_colorbox_image_style' => '',
            'slideshow_colorbox_slideshow' => '',
            'slideshow_colorbox_slideshow_speed' => '4000',
            'slideshow_colorbox_speed' => '350',
            'slideshow_colorbox_transition' => 'elastic',
            'slideshow_controls' => 0,
            'slideshow_field_collection_image' => '',
            'slideshow_fx' => 'fade',
            'slideshow_image_style' => 'sf_main',
            'slideshow_link' => 'colorbox',
            'slideshow_order' => '',
            'slideshow_pager' => 'image',
            'slideshow_pager_image_style' => 'sf_thumb',
            'slideshow_pause' => 0,
            'slideshow_speed' => '1000',
            'slideshow_timeout' => '0',
          ),
          'type' => 'slideshow',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'field_slideshow',
          'settings' => array(
            'slideshow_caption' => '',
            'slideshow_caption_link' => '',
            'slideshow_carousel_circular' => 0,
            'slideshow_carousel_image_style' => '',
            'slideshow_carousel_scroll' => '1',
            'slideshow_carousel_speed' => '500',
            'slideshow_carousel_vertical' => 0,
            'slideshow_carousel_visible' => '3',
            'slideshow_colorbox_image_style' => '',
            'slideshow_colorbox_slideshow' => '',
            'slideshow_colorbox_slideshow_speed' => '4000',
            'slideshow_colorbox_speed' => '350',
            'slideshow_colorbox_transition' => 'elastic',
            'slideshow_controls' => 0,
            'slideshow_controls_position' => 'after',
            'slideshow_field_collection_image' => '',
            'slideshow_fx' => 'fade',
            'slideshow_image_style' => 'thumbnail',
            'slideshow_link' => 'content',
            'slideshow_order' => '',
            'slideshow_pager' => '',
            'slideshow_pager_image_style' => '',
            'slideshow_pager_position' => 'after',
            'slideshow_pause' => 0,
            'slideshow_speed' => '1000',
            'slideshow_timeout' => '0',
          ),
          'type' => 'slideshow',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_images',
      'label' => 'Images',
      'required' => 0,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'node-product_detail-field_product'.
  $fields['node-product_detail-field_product'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_product',
      'foreign keys' => array(
        'product_id' => array(
          'columns' => array(
            'product_id' => 'product_id',
          ),
          'table' => 'commerce_product',
        ),
      ),
      'indexes' => array(
        'product_id' => array(
          0 => 'product_id',
        ),
      ),
      'locked' => '0',
      'module' => 'commerce_product_reference',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'commerce_product_reference',
    ),
    'field_instance' => array(
      'bundle' => 'product_detail',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'commerce_cart',
          'settings' => array(
            'combine' => 1,
            'default_quantity' => '1',
            'line_item_type' => 0,
            'show_quantity' => 1,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '3',
        ),
        'full' => array(
          'label' => 'hidden',
          'module' => 'commerce_cart',
          'settings' => array(
            'combine' => 1,
            'default_quantity' => '1',
            'line_item_type' => 0,
            'show_quantity' => 1,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '3',
        ),
        'product_grid' => array(
          'label' => 'hidden',
          'module' => 'commerce_cart',
          'settings' => array(
            'combine' => 1,
            'default_quantity' => '1',
            'line_item_type' => 0,
            'show_quantity' => 1,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '3',
        ),
        'product_list' => array(
          'label' => 'hidden',
          'module' => 'commerce_cart',
          'settings' => array(
            'combine' => 1,
            'default_quantity' => '1',
            'line_item_type' => 0,
            'show_quantity' => 1,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_product',
      'label' => 'Products',
      'required' => 0,
      'settings' => array(
        'field_injection' => 1,
        'referenceable_types' => array(
          'product' => 0,
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'commerce_product_reference',
        'settings' => array(
          'autocomplete_match' => 'contains',
          'autocomplete_path' => 'commerce_product/autocomplete',
          'size' => '60',
        ),
        'type' => 'commerce_product_reference_autocomplete',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-product_detail-field_rating'.
  $fields['node-product_detail-field_rating'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_rating',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'vote',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'product_detail',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => 0,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => 'default',
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => '4',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => 0,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => 'default',
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => '2',
        ),
        'product_grid' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'product_list' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_rating',
      'label' => 'Rating',
      'required' => 0,
      'settings' => array(
        'allow_clear' => 0,
        'stars' => '5',
        'target' => 'none',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(
          'widget' => array(
            'fivestar_widget' => 'default',
          ),
        ),
        'type' => 'exposed',
        'weight' => '6',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Images');
  t('Products');
  t('Rating');

  return $fields;
}
