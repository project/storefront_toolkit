<?php
/**
 * @file
 * sf_view_modes.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function sf_view_modes_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_display|product_grid';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_display';
  $ds_fieldsetting->view_mode = 'product_grid';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h3',
        'class' => 'product-title',
      ),
    ),
  );
  $export['node|product_display|product_grid'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_display|product_list';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_display';
  $ds_fieldsetting->view_mode = 'product_list';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'node_link' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|product_display|product_list'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function sf_view_modes_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display|product_grid';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display';
  $ds_layout->view_mode = 'product_grid';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'product:field_image',
        2 => 'product:commerce_price',
        3 => 'field_product',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'product:field_image' => 'ds_content',
      'product:commerce_price' => 'ds_content',
      'field_product' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|product_display|product_grid'] = $ds_layout;

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display|product_list';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display';
  $ds_layout->view_mode = 'product_list';
  $ds_layout->layout = 'ds_3col_equal_width';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'left' => array(
        0 => 'product:field_image',
      ),
      'middle' => array(
        0 => 'title',
        1 => 'node_link',
      ),
      'right' => array(
        0 => 'field_product',
      ),
    ),
    'fields' => array(
      'product:field_image' => 'left',
      'title' => 'middle',
      'node_link' => 'middle',
      'field_product' => 'right',
    ),
    'classes' => array(),
  );
  $export['node|product_display|product_list'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function sf_view_modes_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'product_grid';
  $ds_view_mode->label = 'Product Grid';
  $ds_view_mode->entities = array(
    'commerce_product' => 'commerce_product',
    'node' => 'node',
  );
  $export['product_grid'] = $ds_view_mode;

  $ds_view_mode = new stdClass;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'product_list';
  $ds_view_mode->label = 'Product List';
  $ds_view_mode->entities = array(
    'commerce_product' => 'commerce_product',
    'node' => 'node',
  );
  $export['product_list'] = $ds_view_mode;

  return $export;
}
