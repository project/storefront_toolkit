<?php
/**
 * @file
 * sf_browse_products.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function sf_browse_products_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass;
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'browse_products';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Browse Products Small';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'browse_products',
      'display' => 'page',
      'args' => '',
      'title' => 'Grid',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'browse_products',
      'display' => 'page_1',
      'args' => '',
      'title' => 'List',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Browse Products Small');
  t('Grid');
  t('List');

  $export['browse_products'] = $quicktabs;

  $quicktabs = new stdClass;
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'browse_products_full';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 1;
  $quicktabs->title = 'Browse Products Full';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'browse_products',
      'display' => 'page_2',
      'args' => '',
      'title' => 'Grid',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'browse_products',
      'display' => 'page_3',
      'args' => '',
      'title' => 'List',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Browse Products Full');
  t('Grid');
  t('List');

  $export['browse_products_full'] = $quicktabs;

  return $export;
}
