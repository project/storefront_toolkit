<?php
/**
 * @file
 * sf_browse_products.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sf_browse_products_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sf_browse_products_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}
